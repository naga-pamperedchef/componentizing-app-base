## Using Verdaccio to publish lib to local npm registry

### Start Verdaccio

```
npx verdaccio
```

Note, after starting verdaccio, go to [http://localhost:4873/](http://localhost:4873/) and follow the steps mentioned there to login to the local registry

```
npm adduser --registry http://localhost:4873/

```

### build lib

```
ng build lib
```

### publish to npm registry

```
cd dist/lib

npm publish --registry http://localhost:4873/
```