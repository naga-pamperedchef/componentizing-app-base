import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { LibModule } from '@pc/lib/lib.module';
import { PersonalInfoModule } from '@pc/personal-info/personal-info.module';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    LibModule,
    PersonalInfoModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
