/*
 * Public API Surface of personal-info
 */

export * from './lib/personal-info.service';
export * from './lib/personal-info.component';
export * from './lib/personal-info.module';
