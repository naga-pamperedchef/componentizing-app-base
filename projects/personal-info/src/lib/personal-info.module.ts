import { NgModule } from '@angular/core';
import { PersonalInfoComponent } from './personal-info.component';



@NgModule({
  declarations: [
    PersonalInfoComponent
  ],
  imports: [
  ],
  exports: [
    PersonalInfoComponent
  ]
})
export class PersonalInfoModule { }
