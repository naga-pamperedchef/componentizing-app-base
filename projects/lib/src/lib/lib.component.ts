import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ui-lib',
  template: `
    <p>
      lib - component works!
    </p>
  `,
  styles: [
  ]
})
export class LibComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
